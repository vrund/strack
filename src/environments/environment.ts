// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAhLju0Mde17tGWq98HvGb4FA6B9XqKdO8',
    authDomain: 'strack-e5635.firebaseapp.com',
    databaseURL: 'https://strack-e5635.firebaseio.com',
    projectId: 'strack-e5635',
    storageBucket: 'strack-e5635.appspot.com',
    messagingSenderId: '688943257978'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
