import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ClientsComponent } from './clients/clients.component';
import { ProgramsComponent } from './programs/programs.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { HomeComponent } from './home/home.component';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import { CheckinsComponent } from './checkins/checkins.component';
import { TrainerFeaturesComponent } from './trainer-features/trainer-features.component';
import { TaglineComponent } from './tagline/tagline.component';
import { AboutComponent } from './about/about.component';
import { MobileFeaturesComponent } from './mobile-features/mobile-features.component';
import { PlanBuilderComponent } from './plan-builder/plan-builder.component';
import { PlansComponent } from './plans/plans.component';

const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: '', component: TaglineComponent },
      { path: 'app', component: TrainerFeaturesComponent },
      { path: 'mobile', component: MobileFeaturesComponent },
      { path: 'about', component: AboutComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: SignupComponent },
  { path: 'forgot', component: ForgotPasswordComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      { path: 'overview', component: DashboardHomeComponent },
      { path: 'clients', component: ClientsComponent },
      { path: 'programs', component: ProgramsComponent },
      { path: 'checkins', component: CheckinsComponent },
      { path: 'plan-builder', component: PlanBuilderComponent },
      { path: 'plans', component: PlansComponent }
    ]
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
