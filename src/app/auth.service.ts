import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private afAuth: AngularFireAuth) {}

  isLoggedIn(): boolean {
    const loggedIn = this.afAuth.auth.currentUser !== null ? true : false;
    console.log(this.afAuth.auth.currentUser);
    return loggedIn;
  }
  login() {}

  signUp(email: string, password: string): Promise<any> {
    console.log('creating user');
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  forgotPassword() {}
}
