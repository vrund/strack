export class Exercise {
  name: string;
  type: BODY_PART;
}

export enum BODY_PART {
  Neck = 'Neck',
  Traps = 'Traps',
  Shoulders = 'Shoulders',
  Lats = 'Lats',
  Chest = 'Chest',
  Biceps = 'Biceps',
  Triceps = 'Triceps',
  Middle_Back = 'Middle Back',
  Lower_Back = 'Lower Back',
  Forearms = 'Forearms',
  Abs = 'Abs',
  Quads = 'Quads',
  Hamstrings = 'Hamstrings',
  Glutes = 'Glutes',
  Calves = 'Calves'
}
