import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileFeaturesComponent } from './mobile-features.component';

describe('MobileFeaturesComponent', () => {
  let component: MobileFeaturesComponent;
  let fixture: ComponentFixture<MobileFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
