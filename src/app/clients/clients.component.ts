import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

export interface Client {
  name: string;
  weight: number;
  lost: number;
}

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  data: Client[] = [
    { name: 'Vrund Patel', weight: 170, lost: 60 },
    { name: 'Vrund Patel', weight: 170, lost: 60 },
    { name: 'Vrund Patel', weight: 170, lost: 60 },
    { name: 'Vrund Patel', weight: 170, lost: 60 },
    { name: 'Vrund Patel', weight: 170, lost: 60 },
    { name: 'Vrund Patel', weight: 170, lost: 60 },
    { name: 'Vrund Patel', weight: 170, lost: 60 },
    { name: 'Vrund Patel', weight: 170, lost: 60 },
    { name: 'Vrund Patel', weight: 170, lost: 60 }
  ];
  displayedColumns = ['name', 'weight', 'lost'];
  dataSource = new MatTableDataSource(this.data);

  constructor() {}

  ngOnInit() {}
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}
