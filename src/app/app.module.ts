import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Angular Material imports
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatExpansionModule } from '@angular/material/expansion';

// Firebase imports
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';

// Environments
import { environment } from '../environments/environment';

// Misc
import { FullCalendarModule } from 'ng-fullcalendar';

// Components
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { ProgramsComponent } from './programs/programs.component';
import { ClientsComponent } from './clients/clients.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { TrainerFeaturesComponent } from './trainer-features/trainer-features.component';
import { TaglineComponent } from './tagline/tagline.component';
import { AboutComponent } from './about/about.component';
import { MobileFeaturesComponent } from './mobile-features/mobile-features.component';
import { PlanBuilderComponent } from './plan-builder/plan-builder.component';
import { PlansComponent } from './plans/plans.component';
import { HomeComponent } from './home/home.component';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import { CheckinsComponent } from './checkins/checkins.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    ProgramsComponent,
    ClientsComponent,
    SignupComponent,
    ForgotPasswordComponent,
    HomeComponent,
    DashboardHomeComponent,
    CheckinsComponent,
    TrainerFeaturesComponent,
    TaglineComponent,
    AboutComponent,
    MobileFeaturesComponent,
    PlanBuilderComponent,
    PlansComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    BrowserModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatInputModule,
    MatTabsModule,
    MatTableModule,
    MatFormFieldModule,
    MatExpansionModule,
    FullCalendarModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
