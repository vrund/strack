import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user = {} as User;
  password: string;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {}

  createUser() {
    this.authService
      .signUp(this.user.email, this.password)
      .then(res => {
        if (res) {
          console.log(res);
          this.router.navigate(['/dashboard']);
        }
      })
      .catch();
  }
}
