import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerFeaturesComponent } from './trainer-features.component';

describe('TrainerFeaturesComponent', () => {
  let component: TrainerFeaturesComponent;
  let fixture: ComponentFixture<TrainerFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainerFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
